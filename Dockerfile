FROM centos
MAINTAINER philipkdk <pk@philipk.dk>
ENV container docker

# Update everything..
RUN yum update -y; yum clean all

# Setup done
VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/sbin/init"]

#test